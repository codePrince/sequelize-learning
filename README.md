### 关于 建表/迁移
同步关系如下：
migration ==> table
model ==> table (同时是一个清空数据库行为,只应在设计阶段使用)
同步顺序：
model ==> table //初始化, 主要是关联部分在 model 中定义了
migration ==> table //后续维护