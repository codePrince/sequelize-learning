const db = require('../models')
const { Role } = db

module.exports = {
	async addRole(data){
		const role =  Role.build(data)
		await role.save()
	},
	async addArticle(data){
		const item =  db.Article.build(data)
		await item.save()
	},
	async getArticleOne(id){
		return db.Article.findOne({where: {id}})
	},
	async addCat(data){
		const item =  db.Cat.build(data)
		await item.save()
	},
	async addUser(data){
		db.User.create(data)
	},
	async getCatOne(id){
		return db.Cat.findOne({where: {id},  include: db.Article})
	},
}