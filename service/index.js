const path = require('path')
const fs = require('fs')
const basename = path.basename(__filename)

let service = {}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    let item = require(path.join(__dirname, file))
    let name = file.substring(0, file.length-3)
    service[name] = item
  });

module.exports = service