'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class RelationUserRole extends Model {
    static associate(models) {
    }
  };
  RelationUserRole.init({
    roleId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'RelationUserRole',
  });
  return RelationUserRole;
};