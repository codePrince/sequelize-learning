var koa = require('koa');
var app = new koa();
var createLogger = require('concurrency-logger').default;
const session = require('koa-session');
const koaBody = require('koa-body');
const logger = createLogger({});
const router = require('./controller')
const sessionConfig = require('./config/session')
const {sequelize, Sequelize} = require('./models')

sequelize.doConnect()

app.keys = ['some_secret_hurr'];


const port = 2002

app
  .use(koaBody())
  .use(session(sessionConfig, app))
  .use(router.routes())
  .use(router.allowedMethods())
  .use(logger)
  .listen(port);

console.log(`server listen ${port}`);
