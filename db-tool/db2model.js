const SequelizeAuto = require('sequelize-auto')
const env = process.env.NODE_ENV || 'development';
const { host, username, password, database, dialect, port } = require('../config/db.json')[env]

const options = {
  host,
  dialect,
  directory: 'models',  // 指定输出 models 文件的目录
  port,
  skipTables: ['sequelizemeta','relation_user_role']
}
const auto = new SequelizeAuto(database, username, password, options)

auto.run(err => {
  if (err) throw err
})

